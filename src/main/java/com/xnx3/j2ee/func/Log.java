package com.xnx3.j2ee.func;

import com.xnx3.j2ee.util.ConsoleUtil;

/**
 * 控制台日志打印
 * @author 管雷鸣
 * @deprecated 已废弃，请使用 {@link ConsoleUtil}
 */
public class Log extends ConsoleUtil{
	
}
