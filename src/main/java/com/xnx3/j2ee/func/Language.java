package com.xnx3.j2ee.func;

import com.xnx3.j2ee.util.LanguageUtil;

/**
 * 语言相关，比如当前系统的语言包、显示文字调用等
 * @author 管雷鸣
 * @deprecated 已废弃，请使用 {@link LanguageUtil}
 */
public class Language extends LanguageUtil{
	
}
